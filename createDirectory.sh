#Create a dir and subdir to nameDir (1 to n)
#!/bin/bash
# Prompt the user to enter a name directory
echo "Enter name dir""
read -r name
echo "Enter n:"
read n
# Initialize a counter
counter=1
# Start a while loop to create files
mkdir $name
cd $name
while [ $counter -le $n ]
do
    # Create a filename with the name and extension
    dirname="${name}_${counter}"
    mkdir $dirname
    cd $dirname
    # Increment the counter
    counter=$((counter + 1))
done
echo "Files have been created with the name '$name' and extensions from 1 to 10."
