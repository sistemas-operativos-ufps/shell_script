#Create a shell script that reads a name and creates 10 files with that name whose extension varies from 1 to n.Enter the word ufps to each file.
#!/bin/bash
# Read the name from the user
echo "Enter a name file"
read -r  name
# Check if the name is not empty
if [ -z "$name" ]; then
    echo "Name cannot be empty."
    exit 1
fi
# Loop from 1 to 10 to create files with different extensions
for i in {1..10}
do
   # Generate a filename with extension
    filename="$name.$i.txt"  
 # Add "ufps" to the file
    echo "ufps" > "$filename" 
    echo "Created file: $filename"
done
