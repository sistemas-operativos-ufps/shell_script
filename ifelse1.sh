#Shell script that receives 2 numbers per command line and adds up
#!/bin/bash
# Check if two arguments are provided
if [ $# -ne 2 ]; then
    echo "Usage: add_numbers.sh <number1> <number2>"
    exit 1
fi

# Get the first number from the command-line argument
first_number=$1

# Get the second number from the command-line argument
second_number=$2

# Add the two numbers together
sum=$((first_number + second_number))

# Print the result
echo "The sum of $first_number and $second_number is $sum"
