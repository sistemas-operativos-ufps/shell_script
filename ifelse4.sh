#Checks if a string is a regular file or directory and prints its permissions. 
#!/bin/bash
# Check if the correct number of arguments (1) is provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <file_path>"
    exit 1
fi
# Get the file name from the command-line argument
file_path="$1"
# Check if the file exists
if [ -e "$file_path" ]; then
perm=$(stat -c "%A"  "$file_path")
    if [ -d "$file_path" ]; then
        echo "The file '$file_path' is a directory."
        echo "Permissions: $perm"
    elif [ -f "$file_path" ]; then
        echo "The file '$file_path' is a regular file."
        echo "Permissions: $perm"
    else
        echo "The file '$file_path' exists, but is not a directory or regular file."
    fi
else
    echo "The file '$file_path' does not exist."
fi
