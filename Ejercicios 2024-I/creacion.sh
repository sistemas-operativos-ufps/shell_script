#!/bin/bash
call=$1
function crearD() {
        echo "ingresa el nombre"
        read name
        if [ -e "${name}_1" ];then
                echo "el archivo o carpeta ya existe"
                exit
        fi
        echo "ingresa la cantidad"
        read cantidad
        local counter=1

        while [ $counter -le $cantidad ]
        do
                fn="${name}_${counter}"
                $1 $fn
                counter=$((counter + 1))
        done
                tree
}

if [ "$call" = "-d" ];then
        crearD mkdir
elif [ "$call" = "-c" ];then
        crearD touch
else
        echo "comando invalido"
fi
