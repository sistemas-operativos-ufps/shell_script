#!/bin/bash

# Este script recibe un parámetro con el nombre de un archivo, y lo lee
# línea a línea, la salida es una tabla con el nuúmero de la línea, su 
# contenido y la cantidad de palabras que tiene, además de un mensaje
# con la cantidad de líneas que se leyeron.

# printf se usa para imprimir texto con formato específico, en este caso
# se imprime un tabla con el número de la línea, su contenido y la cantidad
# de palabras que tiene.
# Sintaxis: printf FORMAT [ARGUMENT]
# %s: imprime una cadena
# %-5s: imprime cadena con 5 espacios en blanco, alineado a la izquierda

file=$1

if ! [ $# -eq 1 ]
then 
    echo Error, debe ser un solo argumento
    exit 1
fi

if [ -f "$file" ]
then
    contador=0
    echo "------------------------------------------------------------------"    
    printf "| %-5s | %-70s | %-8s |\n" "Linea" "Contenido" "Palabras"
    echo "------------------------------------------------------------------"
    while IFS= read -r line
    do
        ((contador++))
        words=$(echo "$line" | wc -w)
        printf "| %-5s | %-70s | %-8s |\n" "$contador" "$line" "$words"
    done < "$file"
    echo "------------------------------------------------------------------"
    echo "Terminado, se leyeron $contador líneas"
else
    echo "El archivo $file no existe"
fi