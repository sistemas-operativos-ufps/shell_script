#!/bin/bash

mostrar_ayuda() {
    echo "Uso: $0 [opciones] archivo"
    echo
    echo "Opciones:"
    echo "  -u                Convertir el contenido del archivo a mayúsculas"
    echo "  -l                Convertir el contenido del archivo a minúsculas"
    echo "  -c                Contar las líneas del archivo"
    echo "  -s cadena         Buscar una cadena en el archivo"
    echo "  -h                Mostrar esta ayuda"
}

convertir_mayuscula() {
    local file="$1"
    tr '[:lower:]' '[:upper:]' < "$file"
}

convertir_minuscula() {
    local file="$1"
    tr '[:upper:]' '[:lower:]' < "$file"
}

contar_lineas() {
    local file="$1"
    wc -l < "$file"
}

buscar_cadena() {
    local file="$1"
    local search_string="$2"
    grep -n "$search_string" "$file"
}

verificar_archivo() {
    local file="$1"
    if [[ -f "$file" ]]; then
        return 0 
    else
        echo "El archivo $file no existe."
        return 1 
    fi
}

operation=""
search_string=""

while getopts ":ulcs:h" opt; do
    case ${opt} in
        u)
            operation="uppercase"
            ;;
        l)
            operation="lowercase"
            ;;
        c)
            operation="count"
            ;;
        s)
            operation="search"
            search_string="$OPTARG"
            ;;
        h)
            mostrar_ayuda
            exit 0
            ;;
        \?)
            echo "Opción inválida: -$OPTARG" 1>&2
            exit 1
            ;;
        :)
            echo "La opción -$OPTARG requiere un argumento." 1>&2
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))

if [ $# -eq 0 ]; then
    echo "Debe proporcionar un archivo."
    exit 1
fi

archivo="$1"

if verificar_archivo "$archivo"; then
    case $operation in
        uppercase)
            result=$(convertir_mayuscula "$archivo")
            echo "Resultado:"
            echo "$result"
            ;;
        lowercase)
            result=$(convertir_minuscula "$archivo")
            echo "Resultado:"
            echo "$result"
            ;;
        count)
            result=$(contar_lineas "$archivo")
            echo "Número de líneas en el archivo: $result"
            ;;
        search)
            if [ -z "$search_string" ]; then
                echo "Debe proporcionar una cadena para buscar con la opción -s."
                exit 1
            fi
            result=$(buscar_cadena "$archivo" "$search_string")
            echo "Resultados de la búsqueda:"
            echo "$result"
            ;;
        *)
            echo "Debe proporcionar una opción de operación."
            exit 1
            ;;
    esac
fi