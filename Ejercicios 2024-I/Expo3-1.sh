#Script para la demostracion practica de como usar if, else y elif con operadores aritmeticos 

# "="  -eq equal to
# "!=" -ne not equal to
# "<"  -lt less than
# ">"  -gt greater than
# "<=" -le less than or equal to
# ">"  -ge greater than or equal to

#!/bin/bash

if [ $var1 -eq $var2 ]; then
	echo "Son iguales"
elif test $var1 -gt $var2
then
	echo La variable 1 es mayor que la variable 2
else
	echo La variable 2 es mayor que la variable 1
fi
