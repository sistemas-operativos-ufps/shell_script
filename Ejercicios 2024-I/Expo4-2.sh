#!/bin/bash

#El programa recibe una ruta, la cuál verificara si existe y si es un archivo ordinario o
#un directorio...
#1. En el caso de que sea un archivo ordinario verificara si el usuario que ejecuta el comando posee
#   permisos de ejecución, si no los tiene, le asignará permisos de ejecución a todos los 
#   usuarios 
#2. En el caso de que sea un directorio verificara si el sticky bit se encuentra activo, si no lo esta,
#   se asignarán permisos de escritura a el tipo de usuario otros y actvará el sticky bit
#Si la ruta no es archivo ordinario ni directorio se imprimirá dicha información en pantalla
#Si la ruta especificada no se verifica como existente en primer lugar, se imprimirá dicha información
#en pantalla

path="$1"

#Bloque de código agregado que verifica que el número de argumentos sea 1
if [ $# -ne 1 ]; then
    echo "Modo de uso: $0 <ruta del archivo>"
    exit 1
fi

if [ -e "$path" ]; then 
    if [ -f "$path" ]; then
        if [ -x "$path" ]; then
                echo "El $path ya cuenta con permisos de ejecución"
        else
                chmod a+x "$path"
                echo "Permisos de ejecución asignados al archivo $path"
        fi
    elif [ -d "$path" ]; then
        if [ -k "$path" ]; then
                echo "El directorio $path ya tiene el sticky bit activo"
        else
                chmod o+w "$path"
                chmod o+t "$path"
                echo "Sticky bit activado en el directorio $path"
        fi
    else
        echo "El $path no es un archivo o un directorio"
    fi
else
    echo "La ruta $path no existe"
fi