#!/bin/bash
#Esta línea se utiliza para indicar que este script debe ser ejecutado como Bash.

# El propósito principal de este script es cambiar al directorio proporcionado y eliminar todos los archivos
# con la extensión proporcionada

# Verifica si se proporciona un argumento al script.
if [ -z $1 ]; then
    echo "NO"
    exit 1
else
    # Verifica si el argumento proporcionado es un directorio válido.
    if [ ! -d "$1" ]; then
        echo "Error: La ruta proporcionada no es un directorio válido."
        exit 1
    fi

    # Cambia al directorio proporcionado como primer argumento.
    cd $1
    # Haciendo uso del comando "cd" se cambia del directorio actual al proporcionado en el argumento 1.

    echo "Archivos en tu ruta: "
    # Lista los archivos en el directorio actual.
    ls

    # Pide al usuario que ingrese la extensión de archivo que desea borrar.
    read -p "¿Qué extensión de archivo quieres borrar? " archivo
    # Con "read" se lee una línea de entrada y se asigna esa entrada a la variable archivo para saber qué tipo de archivo se desea eliminar, no sin antes usar el argumento (-p) para imprimir el mensaje entre comillas.

    # Elimina de forma recursiva (-r) y forzada (-f) todos los archivos con la extensión proporcionada en el directorio actual.
    rm -rf *.$archivo
    echo "Archivos en tu ruta:"
    # Lista nuevamente los archivos en el directorio actual después de eliminar los archivos con la extensión especificada.
    ls
fi