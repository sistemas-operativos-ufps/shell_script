#!/bin/bash

# La funcion de este script es para buscar una cadena de texto dentro de un archivo txt, se funciona con el ingreso de 3
argumentos: nombre del archivo en que se buscara, la cadena que se desea buscar, el numero de lineas alrededor

# Verificar que se hayan proporcionado exactamente 3 parámetros
if [ $# -ne 3 ]; then
    echo "Uso: $0 nombre_archivo cadena_busqueda num_lineas"
    exit 1
fi

# Asignar los parámetros a variables
archivo=$1
cadena_busqueda=$2
num_lineas=$3

# Mostrar los valores recibidos
echo "Nombre del archivo: $archivo"
echo "Cadena de búsqueda: $cadena_busqueda"
echo "Número de líneas alrededor: $num_lineas"

# Buscar la cadena en el archivo y mostrar las líneas alrededor, sin distinguir mayúsculas y minúsculas
grep -i -C $num_lineas "$cadena_busqueda" "$archivo"