#!/bin/bash

# El programa recibe un nombre del archivo que se quiera buscar en todos los directorios desde la raiz
# Verifica:
#   1. Si el archivo no existe, muestra mensaje de Error
#   2. Si no se tiene permisos para leer el archivo o directorio, muestra mensaje de advertencia
# Luego de esas verificaciones, mostrará una tabla que contiene información del archivo 
# que se encontró en el directorio.

# Mostrar mensaje de bienvenida
echo "¡Bienvenido al sistema de análisis de archivos de texto!"

# Solicitar el nombre del archivo
read -p "Por favor, ingrese el nombre del archivo de texto: " nombre_archivo

# Buscar el archivo en el sistema
rutas_archivos=$(find / -type f -name "$nombre_archivo" 2>/dev/null)

# Verificar si se encontraron archivos
if [ -z "$rutas_archivos" ]; then
    echo "Error: El archivo '$nombre_archivo' no existe en el sistema."
    exit 1
fi

# Iterar sobre cada ruta de archivo encontrada
echo "Archivos encontrados:"
for ruta_archivo in $rutas_archivos; do
    echo -e "\nArchivo encontrado en: $ruta_archivo"

    #Extraemos el nombre del directorio
    directorio_archivo=$(dirname $ruta_archivo)

    # Verificar si tienes permiso de lectura en el directorio y en el archivo
    if [ ! -r "$ruta_archivo" ] || [ ! -r "$directorio_archivo" ]; then
        echo "Advertencia: No tienes permiso para leer el archivo o su directorio."
        continue
    fi

    # Extraer el ID del usuario del propietario del directorio
    # El comando `stat -c '%u'` obtiene el ID del usuario del propietario del directorio especificado.
    id_usuario=$(stat -c '%u' $directorio_archivo)

    # Extraer los permisos del directorio
    # El comando `stat -c '%A'` obtiene los permisos del directorio especificado en formato octal.
    permisos_directorio=$(stat -c '%A' $directorio_archivo)

    #Obtenemos la información del archivo con stat y formatearla
    informacion=$(stat "$ruta_archivo")

    # Mostrar la información del archivo con separación entre campos
    echo -e "Información del archivo:\n"

    # Establecer el separador de campo como nueva línea: IFS=$'\n'
    # Leer la información del archivo línea por línea en un array llamado `stat_ans`
    # El comando `read -r -d '' -a stat_ans < <(stat "$ruta_archivo"; printf '\0')` lee la salida del comando `stat` línea por línea y la almacena en el array `stat_ans`.
    # El caracter nulo (`\0`) se utiliza como delimitador de registro.
    IFS=$'\n' read -r -d '' -a stat_ans < <(stat "$ruta_archivo"; printf '\0');

    # Encabezados de la tabla
    printf "%-20s%-40s\n" "--------------------" "----------------------------------------"
    printf "|%-20s|%-40s\n" "Atributo" "Valor"
    printf "%-20s%-40s\n" "--------------------" "----------------------------------------"

    for line in "${stat_ans[@]}"; do
        #Separar la línea en atributo y valor
        attribute=$(echo "$line" | cut -d: -f1)
        value=$(echo "$line" | cut -d: -f2)

        #Imprimir la fila
        printf "|%-20s|%-40s\n" "$attribute" "$value"
        printf "%-20s%-40s\n" "--------------------" "----------------------------------------"
    done

    #Imprime las variable que obtuvimos del directorio
    printf "|%-20s|%-40s\n" "ID" "$id_usuario"
    printf "%-20s%-40s\n" "--------------------" "----------------------------------------"
    printf "|%-20s|%-40s\n" "Directory permissions" "$permisos_directorio"
    printf "%-20s%-40s\n" "--------------------" "----------------------------------------"
done

# Mostrar mensaje de despedida
echo "¡Gracias por usar el sistema de análisis de archivos de texto! ¡Adiós!"