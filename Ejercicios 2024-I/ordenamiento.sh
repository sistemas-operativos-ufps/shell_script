#!/bin/bash

# Verificar que se hayan pasado tres argumentos
if [ "$#" -ne 3 ]; then
    echo "Uso: $0 cadena1 cadena2 cadena3"
    exit 1
fi

cadena1=$1
cadena2=$2
cadena3=$3

# Ordenar las cadenas utilizando condicionales
if [[ "$cadena1" < "$cadena2" && "$cadena1" < "$cadena3" ]]; then
    primero=$cadena1
    if [[ "$cadena2" < "$cadena3" ]]; then
        segundo=$cadena2
        tercero=$cadena3
    else
        segundo=$cadena3
        tercero=$cadena2
    fi
elif [[ "$cadena2" < "$cadena1" && "$cadena2" < "$cadena3" ]]; then
    primero=$cadena2
    if [[ "$cadena1" < "$cadena3" ]]; then
        segundo=$cadena1
        tercero=$cadena3
    else
        segundo=$cadena3
        tercero=$cadena1
    fi
else
    primero=$cadena3
    if [[ "$cadena1" < "$cadena2" ]]; then
        segundo=$cadena1
        tercero=$cadena2
    else
        segundo=$cadena2
        tercero=$cadena1
    fi
fi

echo "Cadenas ordenadas: $primero, $segundo, $tercero"
