#!/bin/bash

# El script descarga un recurso de internet y muestra su tamaño en KiB
# y su tiempo de descarga en ms, este script recibe 3 paramatros:
# 1. URL
# 2. Directorio donde se guardara el archivo
# 3. Nombre del archivo

URL=$1
DIR=$2
FILENAME=$(basename $3)
FILE_OUT=$DIR/$FILENAME

start_time=$(date +%s%N)

wget -q -O $FILE_OUT $URL

let time=$(date +%s%N)-$start_time


if (($? == 0)); then
	echo "DOWNLOADED"

	size=$(stat -c "%s" $FILE_OUT)


	echo SIZE: $(awk "BEGIN { printf \"%.2f\", $size/1024 }") KiB
	echo TIME: $((time/1000000)) ms
fi
