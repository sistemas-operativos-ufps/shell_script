#Script que revisa si un usuario se excede de un limite de almacenamiento. Si se excede lista de forma ordenada los ficheros y directorios.

#!/bin/bash

if [ $# -ne 2 ]; then
	echo "Debes pasar dos parametros: <usuario> <limite de almacenamiento>"
	exit 1
fi

user=$1
limit=$2

id "$user"

if [ $? -eq 0 ]; then
	path=$(grep $user /etc/passwd | cut -d ":" -f6)
	if [ -r $path ]; then
		disk_usage=$(du -sk $path | cut -f1)
		echo "El usuario $user esta utilizando $disk_usage KB de almacenamiento"
		if [ $disk_usage -ge $limit ]; then
			echo "Se ha excedido el limite"
			du -ah $path | sort -rh
		else
			echo "Se encuentra en el umbral"
		fi
	fi
fi
