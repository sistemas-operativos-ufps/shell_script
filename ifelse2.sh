
#a shell script in Bash that receives three numbers as command line arguments and sorts them in ascending order using #if statements.

#!/bin/bash
# Check if the user provided exactly 3 command line arguments
if [ $# -ne 3 ]; then
    echo "Usage: $0 <number1> <number2> <number3>"
    exit 1
fi
# Assign the command line arguments to variables
num1=$1
num2=$2
num3=$3
# Sort the numbers in ascending order using if statements
if [ $num1 -gt $num2 ]; then
    # Swap num1 and num2
    temp=$num1
    num1=$num2
    num2=$temp
fi
if [ $num2 -gt $num3 ]; then
    # Swap num2 and num3
    temp=$num2
    num2=$num3
    num3=$temp
fi
if [ $num1 -gt $num2 ]; then
    # Swap num1 and num2 again
    temp=$num1
    num1=$num2
    num2=$temp
fi
# Print the sorted numbers
echo "Sorted numbers (ascending order): $num1 $num2 $num3"
