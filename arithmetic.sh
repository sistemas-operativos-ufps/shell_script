#Shell Script arithmetic #Operations                                                                                       
#!/bin/bash
# This script prints a greeting to the user.
name="UFPS"
echo "Hello, $name!"
a=4
# Addition
sum=$((10 + a))
echo "Sum: $sum"
# Subtraction
difference=$((30 - 15))
echo "Difference: $difference"
# Multiplication
product=$((5 * 4))
echo "Product: $product"
# Division
quotient=$((12/3))
echo "Quotient: $quotient"
# Modulo
remainder=$((7 % 4))
echo "Remainder: $remainder"
#Exponentiation
expo=$((3 ** a))
echo "Expo: $expo"
a=$((8+4))
echo "$a"
