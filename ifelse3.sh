#Checks if two strings passed as arguments are equal. 
#Remember to execute: ./ifelse3.sh "UFPS" "ufps"
#!/bin/bash
# Check if exactly two command-line arguments were provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <string1> <string2>"
    exit 1
fi
string1="$1"
string2="$2"
if [ "$string1" = "$string2" ]; then
    echo "The strings are equal."
else
    echo "The strings are not equal."
fi
