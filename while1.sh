#Create a shell script that reads a name and creates 10 files with that name whose extension varies from 1 to n. Enter the word ufps to each file.
#!/bin/bash
# Prompt the user to enter a name
echo "Enter name file"
read -r name
# Initialize a counter
counter=1
# Start a while loop to create files
while [ $counter -le 10 ]
do
    # Create a filename with the name and extension
    filename="${name}_${counter}.txt"
    # Use echo to write "ufps" to the file
    echo "ufps" > "$filename"
    # Increment the counter
    counter=$((counter + 1))
done
echo "Files have been created with the name '$name' and extensions from 1 to 10."
